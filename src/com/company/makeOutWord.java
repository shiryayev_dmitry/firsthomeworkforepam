package com.company;

public class makeOutWord {
    public static void main(String[] args) {
        makeOutWord ss = new makeOutWord();
        System.out.println(ss.makeOutWord("<<>>", "Yay"));
        System.out.println(ss.makeOutWord("<<>>", "WooHoo"));
        System.out.println(ss.makeOutWord("[[]]", "word"));
    }

    public String makeOutWord(String out, String word) {
        return out.substring(0,2)+word+out.substring(2,4);
    }

}
