package com.company;

public class firstHalf {
    public static void main(String[] args) {
        firstHalf ss = new firstHalf();
        System.out.println(ss.firstHalf("WooHoo"));
        System.out.println(ss.firstHalf("HelloThere"));
        System.out.println(ss.firstHalf("abcdef"));
    }

    public String firstHalf(String str) {
        return str.substring(0,(str.length()/2));
    }

}
